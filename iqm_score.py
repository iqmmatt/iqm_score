from datetime import datetime, timedelta
import numpy as np

"""
Short file holding all the information necessary to calculate the IQM Score.

This is a preliminary approach to the score. Much of this math will likely
change after beta testing. At present, winrate is likely to offset cpc and cpm,
as winrate will generally increase (good) as cpm and cpc go up (bad)
"""

"""
BEGIN CONSTANTS
in a later version of this metric, we will probably derive constants from our
own data. At present, I have researched and gathered industry estimates for
advocacy campaigns.
"""

mctr = 0.52
mcpm = 1.50
mcpc = 0.62

"""
BEGIN FUNCTIONS
"""
def ctrx(ctr, mctr):
    """
    ctr = float, clickthrough rate
    mctr = float, median clickthrough rate

    Returns ctr / (2 x industry average)
    """
    return ctr / (2 * mctr)

def cpmx(cpm, mcpm):
    """
    cpm =   float, cost per thousand impressions (CPM) OR cost per click (CPC)
    mcpm =  float, industry mean cost per thousand impressions (CPM) OR
            indutry mean cost per click (CPC)

    returns portion of score for cost per thousand or cost per click (same
    equation used for both metrics)

    equation (2 x industry standard - campaign cost) / (2 x industry standard)
    """
    num = 2 * mcpm - cpm
    den = 2 * mcpm
    return num / den

def spendx(spend, goal):
    """
    spend = float, the average spend per day on the campaign
    goal = the intended daily spend for the campaign

    returns spend portion of the IQM score. Score will be linearly penalized for
    being over OR under the goal rate
    """
    diff = abs(goal - spend)
    return (goal - diff) / goal

def iqm_score(cpm, ctr, winrate, cpc, spend = None, spend_goal = None):
    """
    cpm = float, campaign cost per thousand impressions
    ctr = float, campaign clickthrough rate
    winrate = float, campaign winrate
    cpc = float, campaign cost per click
    spend = float, average daily spend for the campaign
    spend_goal = float, targeted daily spend rate for the campaign

    Returns tuple: IQM score (float), individual scores (list of floats)
    """

    # Creating initial list of individual values
    ivals = [
        winrate,
        cpmx(cpm, mcpm),
        ctrx(ctr, mctr),
        cpmx(cpc, mcpc)
        ]

    # Adding spend metric if data is present
    if spend != None:
        ivals.append(spendx(spend, spend_goal))

    # Storing list length
    n = len(ivals)

    # Capping each value at 1 or 0
    for i in range(n):

        if ivals[i] > 1:
            ivals[i] = 1

        if ivals[i] < 0:
            ivals[i] = 0

    # Getting multiplier to normalize score to 10 point scale
    mult = 10 / (n)

    return round((sum(ivals)) * mult, 1), ivals

def targeting_score(impressions, tot_population, pop_reached, avg_imp_goal,
                    tot_imp_goal, tot_budget, budget_spent):
        """
        impressions =   int, number of impressions total among the targeted
                        population during the campaign
        tot_population = int, number of people in the targeted population
        i_goal      =    int, number of total impressions planned for the entire
                        campaign
        pop_reached = int, unique population reached
        avg_imp_goal= float, goal for average impressions per user
        tot_imp_goal= int, goal for total impressions from the campaign
        tot_budget  = float, total sum of money allocated for the campaign
        budget_spent= float, total sum of money spent on the campaign

        returns a score for audience targeting on a 10-point scale
        """

        # # Getting total length of campaign in days
        # length = (end_date - start_date).days
        #
        # # Getting number of days elapsed in the campaign
        # elapsed = (datetime.now() - start).days
        #
        # completion = elapsed / length
        """Above section commented out to refocus on budget"""

        completion = budget_spent / tot_budget

        # Getting current average impressions
        avg_i = impressions / pop_reached

        # Getting expected average impressions at this point in the campaign
        exp_i = avg_imp_goal * completion

        distance = abs(exp_i - avg_i)
        a_imp_score = (exp_i - distance) / exp_i

        if a_imp_score < 0:
            a_imp_score = 0



        exp_reach = 1 - np.exp(-impressions / tot_population) #This constant to be updated
        print('expected reach', exp_reach)
        act_reach = pop_reached / tot_population
        print('actual reach', act_reach)
        reach_score = act_reach / exp_reach

        if reach_score > 1:
            reach_score = 1

        # Calculating score for total impressions
        exp_ti = tot_imp_goal * completion
        imp_score = impressions / exp_ti

        if imp_score > 1:
            imp_score = 1

        tot_score = imp_score * 2.5 + a_imp_score * 2.5 + reach_score *5

        scores = [tot_score, imp_score, a_imp_score, reach_score]
        scores = [round(x, 1) for x in scores]


        print('tot_score', 'imp_score', "a_imp_score", 'reach_score')
        return scores
